import re

def openFile(path: str):
    """
    Accept the file path as input, open it and returns the text.
    If the file is not found it returns an error.
    """
    f = None
    try:
        f = open(path, 'r', encoding="utf8")
        return f.read()
    except FileNotFoundError:
        print(f"Log error: {repr(FileNotFoundError)}")
    finally:
        if f is not None:
            f.close()


def parseInList(text):
    """
    Accept text as input, parses it by words,
    and enters it element-by-element in the list.
    """
    try:
        return list(map(str, text.split()))
    except AttributeError:
        print(f"Log error: {repr(AttributeError)}")


def deleteSpecialSymbols(text: str):
    """
    Delete special symbols in text
    """
    return re.sub(r"[!@#%&,{}.—]", "", text)


def printList(wordList: list):
    """
    Print list in console
    """
    try:
        for i in range(len(wordList)):
            print(f"Index: {i} Item: {wordList[i]}")
    except TypeError:
        print(f"Log error: {repr(TypeError)}")


fname = "text.txt"
text = openFile(fname)
wordList = parseInList(deleteSpecialSymbols(text))
printList(wordList)
